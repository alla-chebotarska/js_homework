
/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/
function Hamburger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = 'small';
Hamburger.SIZE_LARGE = 'large';
Hamburger.STUFFING_CHEESE = 'cheese';
Hamburger.STUFFING_SALAD = 'salad';
Hamburger.STUFFING_POTATO = 'potato';
Hamburger.TOPPING_MAYO = 'mayo';
Hamburger.TOPPING_SPICE = 'spice';

/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/
Hamburger.prototype.addTopping = function (topping) {
    if (this.topping.includes(topping)) {
        throw new HamburgerException("This topping already exists!");
    }
    this.topping.push(topping);
}

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (!this.topping.includes(topping)) {
        throw new HamburgerException("This topping is not added!");
    }
    this.topping.splice(this.topping.indexOf(topping), 1);
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.topping;
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let price = 0;
    let sizePrice = 0;
    let stuffingPrise = 0;
    let toppingPrise = 0;
    switch (this.size) {
        case Hamburger.SIZE_SMALL: sizePrice = 50;
            break;
        case Hamburger.SIZE_LARGE: sizePrice = 100;
            break;
    };

    switch (this.stuffing) {
        case Hamburger.STUFFING_CHEESE: stuffingPrise += 10;
            break;
        case Hamburger.STUFFING_SALAD: stuffingPrise += 20;
            break;
        case Hamburger.STUFFING_POTATO: stuffingPrise += 15;
            break;
    };

    this.topping.forEach(element => {
        switch (element) {
            case Hamburger.TOPPING_MAYO: toppingPrise += 20;
                break;
            case Hamburger.TOPPING_SPICE: toppingPrise += 15;
                break;
        }
    });
    price = sizePrice + stuffingPrise + toppingPrise;
    return price;
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    let calories = 0;
    let sizeCalories = 0;
    let stuffingCalories = 0;
    let toppingCalories = 0;
    switch (this.size) {
        case Hamburger.SIZE_SMALL: sizeCalories = 20;
            break;
        case Hamburger.SIZE_LARGE: sizeCalories = 40;
            break;
    };

    switch (this.stuffing) {
        case Hamburger.STUFFING_CHEESE: stuffingCalories += 20;
            break;
        case Hamburger.STUFFING_SALAD: stuffingCalories += 5;
            break;
        case Hamburger.STUFFING_POTATO: stuffingCalories += 10;
            break;
    };

    this.topping.forEach(element => {
        switch (element) {
            case Hamburger.TOPPING_MAYO: toppingCalories += 5;
                break;
            case Hamburger.TOPPING_SPICE: toppingCalories += 0;
                break;
        }
    });
    calories = sizeCalories + stuffingCalories + toppingCalories;
    return calories;
}


/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException(message) {
    this.message = message;
}