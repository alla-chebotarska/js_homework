import React from 'react';

import './footerItem.css';

const FooterItem = (props) => {
    return (
        <li className={`footer-list-item ${props.active ? 'footer-active-item' : '' }`}><a>{props.children}</a></li>
    )
}

export default FooterItem;