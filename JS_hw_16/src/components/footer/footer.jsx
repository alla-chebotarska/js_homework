import React from 'react';
import FooterItem from './footerItem/footerItem';

import './footer.css';

const Footer = () => {
    return (
        <div className='footer'>
            <ul className='footer-list'>
                <FooterItem>Ramda</FooterItem>
                <FooterItem active>Why Ramda?</FooterItem>
                <FooterItem>What's different</FooterItem>
                <FooterItem>Introductions</FooterItem>
                <FooterItem>Philosophy</FooterItem>
            </ul>
            <div className='copyright'>Copyright © 2018</div>
        </div>
    )
}

export default Footer;