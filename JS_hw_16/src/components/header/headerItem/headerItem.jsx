import React from 'react';

import './headerItem.css';

const HeaderItem = (props) => {
    return (
    <li className={`header-list-item ${props.active ? 'header-list-item-active' : ''}`}><a>{props.children}</a></li>
    )
}

export default HeaderItem;