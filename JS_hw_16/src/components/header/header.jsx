import React from 'react';


import './header.css';
import HeaderItem from './headerItem/headerItem';

const Header = () => {
    return(
        <div className='header'>
            <ul className='header-list'>
               <HeaderItem>Functional Programming</HeaderItem>
               <HeaderItem active>Ramda</HeaderItem>
               <HeaderItem>Fantasy Land Spec</HeaderItem>
               <HeaderItem>Node.js for all</HeaderItem>
            </ul>
        </div>
    )
}

export default Header;