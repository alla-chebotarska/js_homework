import React from 'react';
import LeftMenu from './leftMenu/leftMenu';
import Content from './content/content'

const MainContent = () => {
    return(
        <div className="main-content-container">
            <LeftMenu/>
            <Content/>
        </div>
    )
}

export default MainContent;