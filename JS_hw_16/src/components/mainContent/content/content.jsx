import React from 'react';
import MainImage from './mainImage/mainImage';
import Article from './article/article'

import '../mainContent.css';
import './content.css'

const Content = () => {
    return(
        <div className='content'>
            <MainImage/>
            <Article/>
        </div>
    )
}

export default Content;