import React from 'react';
import MenuItem from './menuItem/menuItem';

import '../mainContent.css';
import './leftMenu.css';

const LeftMenu = () => {
    return(
        <div className='left-menu'>
            <ul className='left-menu-list'>
                <MenuItem>Ramda</MenuItem>
                <MenuItem active>Why Ramda?</MenuItem>
                <MenuItem>What's Different?</MenuItem>
                <MenuItem>Introductions</MenuItem>
                <MenuItem>Philosophy</MenuItem>
            </ul>
        </div>
    )
}

export default LeftMenu;