import React from 'react';

import './menuItem.css';

const MenuItem = (props) => {
    return (
        <li className={`left-menu-list-item ${props.active ? "left-memu-item-activ" : ""}`} >{props.children}</li>
    )
}

export default MenuItem;