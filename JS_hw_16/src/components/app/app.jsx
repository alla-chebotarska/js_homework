import React from "react";
import Icon from '../icon/icon';
import Header from '../header/header';
import MainContent from "../mainContent/mainContent";
import Footer from '../footer/footer';

const App = () => {
    return (
        <div>
            <Icon/>
            <Header/>
            <MainContent/>
            <Footer/>
        </div>
    )
}

export default App;
