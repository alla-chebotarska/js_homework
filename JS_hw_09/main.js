const img = document.getElementById("image");


const images = [ "./images/1.jpg",  "./images/2.jpg",  "./images/3.jpg", "./images/4.jpg",  "./images/5.jpg",  "./images/6.jpg"];
img.src = images[images.length - 1];

let nextImage = 0;

const next = function(){
    img.src = images[Math.abs(nextImage++ % images.length)];
}

const previous = function(){
        img.src = images[Math.abs(nextImage-- % images.length)];
}