const discountBtn = document.getElementById("discountBtn");
console.log()

discountBtn.onmousemove = function (event) {
    let newX = event.clientX + discountBtn.offsetWidth;
    let newY = event.clientY + discountBtn.offsetHeight;

    if (newX + discountBtn.offsetWidth > window.innerWidth || newY + discountBtn.offsetHeight > window.innerHeight) {
        newX = 0;
        newY = 0;    
    } 

    discountBtn.style.left = newX + 'px';
    discountBtn.style.top = newY + 'px';
    

}