const main = document.getElementById("main");
const paragraphs = document.getElementsByClassName("sample-text");

main.onkeydown = function (event) {
    switch (event.keyCode) {
        case 82:  //key R
            for (let p of paragraphs) {
                p.style.color = "red";
            }
            break;
        case 71: //key G
            for (let p of paragraphs) {
                p.style.color = "green";
            }
            break;
        case 66: //key B
            for (let p of paragraphs) {
                p.style.color = "blue";
            }
            break;
    }
}




