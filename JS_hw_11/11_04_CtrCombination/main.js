const main = document.getElementById("main");
const result = document.getElementById("result");

function clear() {
    result.innerHTML = "";
}

main.onkeydown = function (event) {
    if (event.ctrlKey) {
        switch (event.code) {
            case "KeyA":
                result.innerHTML = "Ctrl + A";
                event.preventDefault();
                break;
            case "KeyS":
                if (event.shiftKey) {
                    result.innerHTML = "Ctrl + Shift + S";
                } else {
                    result.innerHTML = "Ctrl + S";
                }
                event.preventDefault();
                break;
                default: clear();
        }
    } else {
        clear();
    }
}


