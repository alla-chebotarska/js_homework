const input = document.getElementById("input");
const saveBtn = document.getElementById("saveBtn");
let savedInputValue;

const saveInputValue = function(){
    savedInputValue = input.value;
}

saveBtn.addEventListener("click", saveInputValue);

window.addEventListener("beforeunload", function (event) {
   if(input.value !== savedInputValue){ //check if current value was changed after saving
    event.returnValue = "Please save all changes before close the tab";
   } 
});

