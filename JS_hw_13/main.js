const ajax = new XMLHttpRequest();
let result = document.getElementById("result");

ajax.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");

ajax.onreadystatechange = () => {
    if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
        let filteredArray = JSON.parse(ajax.responseText).filter((element) => { //filter the rate more than 25
            return element.rate > 25
        });
        for (let i = 0; i < filteredArray.length; i++) { //add evere element from filtered array to the table
            result.innerHTML += `<tr><td>${filteredArray[i].txt}</td><td>${filteredArray[i].cc}</td> 
            <td>${filteredArray[i].rate}</td></tr>`;
        }
    } else {
        console.log("Ajax request did not complete");
    }
}

ajax.send()