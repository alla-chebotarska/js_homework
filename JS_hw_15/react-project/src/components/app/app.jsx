import React from 'react'
import Header from '../header/header'
import Image from '../image/image'
import ArticleHeader from '../articleHeader/articleHeader'
import Paragraph from '../paragraph/paragraph'
import Button from '../button/button'
import Footer from '../footer/footer'
import "./app.css"

const App = () =>{
     return(
         <div>
             <Header/>
             <div className = "article">
             <Image/>
             <ArticleHeader/>
             <Paragraph text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
             Maecenas sed nunc sodales, tristique risus eget, vestibulum ante. Morbi aliquet 
             cursus elit non molestie. Phasellus et ultrices elit, nec tempor purus. 
             Donec finibus in sapien ac bibendum. Morbi at dui lectus. Sed euismod urna venenatis elit rutrum, 
             at interdum felis tristique. Mauris vel ullamcorper eros. Maecenas nec massa purus.
             " />
             <Paragraph text="Quisque varius, arcu non fringilla suscipit, tortor risus interdum lorem, 
             in ornare ligula dui non leo. Fusce quis nibh quis orci ornare vestibulum ac eu nunc. 
             Suspendisse libero leo, ullamcorper interdum lobortis ut, dapibus imperdiet dui. Proin nulla purus, 
             lacinia quis neque sit amet, congue porttitor nisl. Suspendisse vel arcu id enim egestas pharetra quis et purus. 
             Morbi tincidunt a mi nec volutpat. Aliquam eu aliquam purus. Vivamus interdum arcu in semper euismod. 
             Nulla rutrum porta enim nec aliquam. Nam in sagittis mi. Mauris dapibus scelerisque ante non fringilla. 
             Mauris tincidunt at risus sit amet interdum. Cras eleifend cursus faucibus. In hac habitasse platea dictumst. 
             Aliquam vitae mi et lectus facilisis vehicula vitae consequat arcu. Duis leo magna, varius eget varius sed, 
             sollicitudin sit amet diam.Sed id odio nec tortor tempor scelerisque sit amet et sapien. Donec ipsum justo, 
             feugiat id ex sit amet, consectetur tincidunt justo. Vivamus sed purus ipsum. Integer dictum elit 
             in tempor condimentum. Proin vitae purus pulvinar, condimentum lectus non, eleifend orci. 
             Donec neque nulla, dapibus in sollicitudin pretium, rutrum eu sem. Pellentesque ac orci iaculis,
              varius odio sit amet, fringilla ante. Aliquam in pharetra elit, eu feugiat sem. Integer eu gravida tortor. 
              Donec mollis, metus id ullamcorper efficitur, nulla libero congue libero, nec rhoncus lorem diam vel ante. 
              Donec semper non urna a dignissim."/>
              <Button />
              </div>
              <Footer text="Copyright by phototime - all right reserved" />
         </div>
     )
 }

 export default App;