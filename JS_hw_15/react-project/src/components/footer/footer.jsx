import React from 'react'
import './footer.css'

const Footer = (props) => {
    return (
        <p className="footer">{props.text}</p>
    )
}

export default Footer;