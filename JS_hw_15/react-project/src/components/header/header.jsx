import React from "react"
import "./header.css"

const Header = (props) => {
    return (
        <header className="header">
            <ul className="header-list">
                <li>home</li>
                <li>Photoappp</li>
                <li>design</li>
                <li>download</li>
            </ul>
        </header>
    )
}

export default Header;