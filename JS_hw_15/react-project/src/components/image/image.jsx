import React from 'react'
import mainImage from './mainImage.png'
import "./image.css"

const Image = () => {
    return(
      <div>
          <img src={mainImage} alt="main"/> 
      </div> 
    )
    
}

export default Image;