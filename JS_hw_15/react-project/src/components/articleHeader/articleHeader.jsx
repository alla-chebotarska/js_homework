import React from "react"
import './articleHeader.css'

const ArticleHeader = () => {
    return (
        <h2>Represent your life with a simple photo</h2>
    )
}

export default ArticleHeader;