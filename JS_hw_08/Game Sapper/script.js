let cellsArray = new Array();
const numberOfCellsInRow = 8;
const cellWidth = 40;
const cellHeight = 40;
const cellMargin = 2;
let gameOver = false;
let clickOnBtnCount = 0;
let mines = 10;

const mineCounter = document.getElementById("mineCounter");


// The function is generate  10 mine
function generatMines(arrayForModify) {
    let count = 10;
    for (let i = 0; i < count; i++) {
        let x = Math.floor(Math.random() * numberOfCellsInRow);
        let y = Math.floor(Math.random() * numberOfCellsInRow);
        if (arrayForModify[x][y].isMine) {
            count++;
        }
        arrayForModify[x][y].isMine = true;
    }
    return arrayForModify;
}

//modifi mine counter for neibor cells
function modifyMineCounter(arrayForModify) {
    for (let i = 0; i < arrayForModify.length; i++) {
        for (let j = 0; j < arrayForModify.length; j++) {
            if (arrayForModify[i][j].isMine == true) {
                update(arrayForModify, i, j - 1);
                update(arrayForModify, i, j + 1);
                update(arrayForModify, i - 1, j - 1);
                update(arrayForModify, i - 1, j);
                update(arrayForModify, i - 1, j + 1);
                update(arrayForModify, i + 1, j - 1);
                update(arrayForModify, i + 1, j);
                update(arrayForModify, i + 1, j + 1);

            }
        }
    }
}

function print(array) {
    for (let i = 0; i < array.length; ++i) {
        let str = "";
        for (let j = 0; j < array.length; ++j) {
            if (array[i][j].isMine) {
                str += '* ';
            } else {
                str += `${array[i][j].count} `;
            }
        }
        console.log(str);
    }
}

function update(cells, i, j) {
    if (i >= 0 && i < cells.length && j >= 0 && j < cells.length) {
        cells[i][j].count += 1;
    }
}

function createGameField(rows, columns) {
    for (let i = 0; i < rows; i++) {
        cellsArray[i] = new Array();
        for (let j = 0; j < columns; j++) {
            cellsArray[i][j] = {
                isOpen: false,
                isMine: false,
                count: 0
            }

        }
    }
    generatMines(cellsArray);
    modifyMineCounter(cellsArray);
    print(cellsArray);
    return cellsArray;
}

function createLineBreak() {
    const br = document.createElement("br");
    document.getElementById("gameArea").appendChild(br);
}

function startNewGame() {
    clickOnBtnCount += 1;
    if (clickOnBtnCount === 1) { // Show button "Start new game after first click on game field"
        addStartNewGameBtn();
    }
}

// Add event listener for every cell
function showContent(event) {
    if (gameOver) {
        return;
    }
    startNewGame();

    event.target.style.backgroundColor = '#a5d6a7';
    event.target.setAttribute("class", "shown");
    if (event.target.value == "*") {
        gameOver = true;
        event.target.style.backgroundColor = '#ff9999';
        const array = document.getElementsByClassName("cell");
        Array.from(array).forEach(item => {
            if (item.value == "*") {
                item.setAttribute("class", "shown");
            }
        })
    }
}

function rightClick(event){
    startNewGame();
    if(event.target.classList.contains("flag")){
        event.target.classList.remove("flag");
        mines +=1;
    }else{
        event.target.classList.add("flag");
        mines -= 1;
    }
    mineCounter.innerHTML = `You should found ${mines} out of 10 mines`;
    event.preventDefault();
}

// Draw Game Field
function drawGameField(cellWidth, cellHeight, cellMargin, arrayWithInformation) {
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            const cell = document.createElement("input");
            cell.setAttribute("style",
                `width: ${cellWidth}px; 
                height: ${cellHeight}px; 
                margin: ${cellMargin}px;`);
            cell.setAttribute("class", "cell");
            cell.setAttribute("type", "button");
            if (arrayWithInformation[i][j].isMine) {
                cell.setAttribute("value", `*`);
            } else {
                cell.setAttribute("value", arrayWithInformation[i][j].count);
            }
            cell.addEventListener("click", showContent);
            cell.addEventListener("contextmenu", rightClick);
            document.getElementById('gameArea').appendChild(cell);
        }
        createLineBreak();
    }
}

function showGameField() {
    document.getElementById("gameArea").innerHTML = "";
    mines = 10;
    mineCounter.innerHTML = `You should found ${mines} out of 10 mines`;
    gameOver = false;
    clickOnBtnCount = 0;
    console.log(clickOnBtnCount);
    createGameField(8, 8);
    drawGameField(cellWidth, cellHeight, cellMargin, cellsArray);

}

showGameField();

// Button to start new game
function addStartNewGameBtn() {
    const startNewGameBtn = document.createElement("input");
    startNewGameBtn.setAttribute("type", "button");
    startNewGameBtn.setAttribute("class", "start-new-game");
    startNewGameBtn.setAttribute("value", "Start new Game");
    startNewGameBtn.addEventListener("click", showGameField);
    document.getElementById("startNewGame").appendChild(startNewGameBtn);
}