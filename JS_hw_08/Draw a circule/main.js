const drawCircleBtn = document.getElementById("drawCircleBtn");

drawCircleBtn.addEventListener("click", drawCircle);

function createLineBreak() {
    const br = document.createElement("br");
    document.body.appendChild(br);
}

function deleteCircle(event) {
    event.target.remove();
}

// drawCircle() thit function create 100 circles with the diameter seted by user
function drawCircle() {
    const circleDiameter = prompt("Please anter the circle diameter in px");
    let color = 0;
    document.body.style.width = (10 * circleDiameter) + "px";
    createLineBreak();
    for (let i = 1; i <= 100; i++) {
        color += 15;
        const circle = document.createElement("div");
        circle.setAttribute("style",
            `background-color: 	hsl(${color}, 100%, 70%); 
        width: ${circleDiameter}px; 
        height: ${circleDiameter}px; 
        border-radius: 50%;
        display: inline-block`);
        circle.addEventListener("click", deleteCircle);
        document.body.appendChild(circle);
    }

}